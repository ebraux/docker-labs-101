# Personnalisation d'une image avec COPY

---
## Objectifs

Créer une image personnalisée :

- A partir d'une image "nginx" 
- Qui intègre une page disponible en local

Cas d'usage : livraison d'un développement web.

---
## Principe 

Pour personnaliser une image :

- Créer le site et le fichier html
- Identifier le dossier utilisé pour le contenu web de l'image Nginx : [https://hub.docker.com/_/nginx](https://hub.docker.com/_/nginx)
- Écrire un fichier de personnalisation : **Dockerfile**
- Utiliser la commande `COPY` pour transférer du contenu vers le conteneur
- Construire une nouvelle image.

---
## Solution

- [Proposition de solution](./dockerfile_copy_solution.md)

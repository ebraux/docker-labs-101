# Orchestration de conteneur : Swarm


## Objectif

Déployer des conteneur en utilisant l'orchestrateur "Swarm" :

- Déploiement
- Montée en charge et redimensionnement

> Pour des raisons pratiques, Swarm va être utilisé sur un seul noeud. Dans la pratique, on utilise une architecture avec plusieurs noeuds :  des managers, et des workers.

## Initialisation de Swarm

Basculer Docker en mode Swarm
```bash
docker swarm init
```

```bash
docker node ls
# ID                            HOSTNAME      STATUS    AVAILABILITY   MANAGER STATUS   ENGINE VERSION
# uw8djoaw3ul7gtR0coc251ow8 *   lab-swarm-1   Ready     Active         Leader           20.10.10
```

---

## Création d'un service 

Lancement d'une application web, et connexion sur le port 8081

```bash
docker service create --name webapp -d --replicas 2 -p 8081:80  nginx
```

Lister les services
```bash
docker service  ls
# ID             NAME       MODE         REPLICAS   IMAGE          PORTS
# 1msjccj3tyfm   webapp   replicated   2/2        nginx:latest   *:8081->80/tcp
```

Afficher les informations sur le service 'webapp' qui vient d'être créé:
```bash
docker service  ps webapp
# ID             NAME         IMAGE          NODE        DESIRED STATE   CURRENT STATE                ERROR     PORTS
# s6vkw4vkcb3g   webapp.1   nginx:latest   lab-swarm-1   Running         Running about a minute ago             
# 00sk6pako1qh   webapp.2   nginx:latest   lab-swarm-1   Running         Running about a minute ago                
```

Afficher les conteneur :
```bash
docker ps
# CONTAINER ID   IMAGE              COMMAND                  CREATED              STATUS    PORTS           NAMES
# 7350fb3190ed   nginx:latest     "/docker-entrypoint.…"   About a minute ago   Up About a minute   80/tcp  webapp.2.00sk6pako1qhepamgyk5jlx0k
# 5159eb960324   nginx:latest     "/docker-entrypoint.…"   About a minute ago   Up About a minute   80/tcp  webapp.1.s6vkw4vkcb3g755tky19ynxv2
```

---
## Tester le fonctionnement du service
Afficher les logs dans un terminal, en mode "follow"
```bash
docker service  logs -f  webapp
```

Dans un autre terminal faire des requêtes avec curl
```bash
curl http://localhost:8081
curl http://localhost:8081
curl http://localhost:8081
curl http://localhost:8081
```

Les conteneur traitent les requêtes chacun leur tour
```bash
# ...
# webapp.1.s6vkw4vkcb3g@ebraux-work    | 10.0.0.2 - - [17/Nov/2021:19:18:17 +0000] "GET / HTTP/1.1" 200 615 "-" "curl/7.68.0" "-"
# webapp.2.00sk6pako1qh@ebraux-work    | 10.0.0.2 - - [17/Nov/2021:19:18:48 +0000] "GET / HTTP/1.1" 200 615 "-" "curl/7.68.0" "-"
# webapp.1.s6vkw4vkcb3g@ebraux-work    | 10.0.0.2 - - [17/Nov/2021:19:18:50 +0000] "GET / HTTP/1.1" 200 615 "-" "curl/7.68.0" "-"
# webapp.2.00sk6pako1qh@ebraux-work    | 10.0.0.2 - - [17/Nov/2021:19:18:52 +0000] "GET / HTTP/1.1" 200 615 "-" "curl/7.68.0" "-"
# ...
```

## Redimensionner le service

Augmenter le nombre de conteneur de 2 à 3
```bash
docker service scale  webapp=3
```

Docker lance un nouveau conteneur
```bash
# webapp scaled to 3
# overall progress: 3 out of 3 tasks 
# 1/3: running   [==================================================>] 
# 2/3: running   [==================================================>] 
# 3/3: running   [==================================================>] 
# verify: Service converged 
```

```bash
docker service ps  webapp
# ID             NAME         IMAGE          NODE          DESIRED STATE   CURRENT STATE            ERROR     PORTS
# s6vkw4vkcb3g   webapp.1   nginx:latest   lab-swarm-1   Running         Running 21 minutes ago             
# 00sk6pako1qh   webapp.2   nginx:latest   lab-swarm-1   Running         Running 21 minutes ago             
# q63mw5st0bg2   webapp.3   nginx:latest   lab-swarm-1   Running         Running 2 minutes ago     
```

Revenir à 2 conteneurs
```bash
docker service scale  webapp=2
```

---
## Ménage

Supprimer le service
```bash
docker service rm webapp
```

Reconfigurer Docker "hors Swarm"
```bash
docker swarm leave --force
```

# Lab Docker et persistance des données

---
## Objectif

Utilisation d'une image alpine pour illustrer la problématique de persistance des données dans un conteneur.


---
## Persistance des données dans un conteneur

### Principe

- Lancer un conteneur en mode daemon
- Se connecter au conteneur, et créer un fichier
- Se déconnecter
- Se reconnecter, et afficher le contenu du fichier


### Lancer un conteneur alpine en mode daemon
```bash
docker run  -t  --name c-vol-lab -d alpine
```

### Créer un fichier dans le conteneur

``` bash
docker exec -it c-vol-lab sh
```

On est alors dans le conteneur, d'où le prompt "root@xxxxxxxxx".

Lister les dossiers à la racine du conteneur. Il n'y a pas de dossier `/datas`

``` bash
ls -l /
# total 56
# drwxr-xr-x    2 root     root          4096 Mar 29 14:45 bin
# drwxr-xr-x    5 root     root           360 Jun  7 11:33 dev
# drwxr-xr-x    1 root     root          4096 Jun  7 11:33 etc
# drwxr-xr-x    2 root     root          4096 Mar 29 14:45 home
# ...
```

Créer le dossier /datas, un fichier de test, et vérifier son contenu
``` bash
mkdir /datas

echo 'Hello-world' > /datas/test.txt
ls
cat /datas/test.txt
```

Puis  se déconnecter du conteneur
``` bash
exit
```

### Afficher le contenu du fichier depuis l'hôte

Afficher le contenu du fichier dan sle conteneur

``` bash
docker exec c-vol-lab cat /datas/test.txt
```

Quand on se déconnecte puis se reconnecte à un conteneur, le fichier est toujours présent.

---
## Persistance en cas d'arrêt du conteneur

### Principe

- Vérifier le contenu du fichier précédemment créé
- Arrêter le conteneur
- Relancer le conteneur
- Vérifier le contenu du fichier

### Arrêter le conteneur, puis le relancer

Afficher le contenu du fichier test.txt
``` bash
docker exec c-vol-lab cat /datas/test.txt
```

Arrêter le conteneur, puis le relancer
``` bash
docker stop c-vol-lab
docker start c-vol-lab
```

Vérifier le contenu du fichier
``` bash
docker exec c-vol-lab cat /datas/test.txt
```

Le fichier est toujours présent.

---
## Persistance en cas de Suppression/relance du conteneur

### Principe

- Vérifier le contenu du fichier précédemment créé
- Supprimer le conteneur
- Relancer le conteneur
- Vérifier le contenu du fichier

### Arrêter le conteneur, puis le relancer

Afficher le contenu du fichier test.txt
``` bash
docker exec  c-vol-lab cat /datas/test.txt
```

Arrêter le conteneur, puis le supprimer, puis le recréer

``` bash
docker stop c-vol-lab
docker rm c-vol-lab
docker run  -t  --name c-vol-lab -d alpine
```

Vérifier le contenu du fichier
``` bash
docker exec  c-vol-lab cat /datas/test.txt
```

Le message 'cat: can't open '/datas/test.txt': No such file or directory' s'affiche.

**Le conteneur a été ré-initialisé, les données modifiées sont perdues.**

---
## Ménage

``` bash
docker stop c-vol-lab
docker rm c-vol-lab
```

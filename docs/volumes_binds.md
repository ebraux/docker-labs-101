# Lab Docker et volumes montés

---
## Objectif

Utilisation d'une image nginx pour illustrer l'utilisation de  volumes montés, pour assurer la persistance des données.

Cas d'usage : publication de contenu développé en local


---
## Principe

Les "volumes montés" permettent d'accéder depuis le conteneur, à des données stockées en dehors du conteneur, sur la machine hôte.
C'est l'option `-v`, syntaxe : `docker run -v /host/directory:/container/directory`.

---
## Création d'un contenu de développement en local

Créer un dossier pour du contenu en cours de développement sur l'hôte

``` bash
mkdir local_dev; cd local_dev
```

Créer un squelette de site
```bash
mkdir mysite;


tee  mysite/index.html <<EOF
<html>
  <body>
    <h1>My Site</h1>
  </body>
</html>
EOF
```

Et vérification
```bash
cat  mysite/index.html
```

---
## Lancement du conteneur en montant un fichier local

Lancer le conteneur en "montant " ce fichier local dans le conteneur homedir de l'utilisateur par défaut du conteneur, en utilisant l'option -v :

``` bash
docker run --name c-my-dev -p 8081:80 -d -v ${PWD}/mysite:/usr/share/nginx/html nginx
```


Et accès au site via un navigateur : [http://localhost:8081](http://localhost:8081)

Ou avec curl
``` bash
curl http://localhost:8081
```

Une page contenant le message "My Site" doit s'afficher.


---
## Vérifier la persistance des données


Arrêter le conteneur, le supprimer  puis le recréer :

``` bash
docker stop c-my-dev
docker rm c-my-dev
docker run --name c-my-dev -p 8081:80 -d -v ${PWD}/mysite:/usr/share/nginx/html nginx
```

Accéder de nouveau au site. 

Le contenu modifié est toujours présent,  car il n'est pas directement lié au conteneur.

Ne pas arrêter le conteneur.

---
## Modification du contenu local 

Modifier le fichier en local, et observer le résultat dans le conteneur

``` bash
tee  mysite/index.html <<EOF
<html>
  <body>
    <h1>My Site !!! </h1>
  </body>
</html>
EOF
```


Accéder de nouveau au site. 

Une page contenant le message "My Site !!! " doit s'afficher. (si besoin rafraîchissez la page)

Le contenu du dossier "mysite" est accessible directement par le conteneur. Les modifications effectuées dans ce dossier sont donc automatiquement prises en compte.

---
## Ménage

Arrêt et suppression du conteneur
``` bash
docker stop c-my-dev
docker rm c-my-dev
```

suppression du dossier du lab
``` bash
cd ..
rm -rf local_dev
```


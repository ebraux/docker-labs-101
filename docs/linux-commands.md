# Quelques commandes Linux



Ce déplacer dans un dossier : `cd` 

- En mode relatif (par rapport au dossier courant)
``` bash
cd mon-dossier
```

- En mode absolu (par rapport au dossier racine du système '/')
``` bash
cd /home/user/mon-dossier
```
Afficher le contenu d'un dossier

- Affichage simple : `ls`
- Affichage en mode 'list' avec des détails : `ls -l` ou `ll` si disponible
- Affichage des fichiers cachés : `ls -a` ou `ls -la`

Afficher le chemin du répertoire courant : `pwd`

Afficher le contenu d'une arborescence : `tee`

---
## Travailler avec des chaînes de caractère et des variables

Afficher une chaine de caractère : `echo "mon text a afficher"`

Afficher le contenu d'une variable : `echo $MA_VARIABLE`


---
## Travailler avec des fichiers

Afficher le contenu d'un fichier : `cat un-fichier.txt`

Afficher le contenu d'un long fichier : `cat un-fichier.txt | less`

- La sortie de la commande cat est redirigée vers la commande less, grâce au pipe '|'
- La commande `less` permet de parcourir le contenu affiché

Afficher le contenu d'un fichier jason : `cat un-fichier.txt | jq`
- La sortie de la commande cat est redirigée vers la commande less, grâce au pipe '|'
- La commande `jq` permet formater l'affichage d'un contenu au format json.
  
Ajouter du contenu dans un ficher :

- Remplacer le contenu existant : `echo "mon contenu" > mon_fichier.txt`
- Ajouter au contenu existant : `echo "mon contenu" >> mon_fichier.txt`

Ajouter du contenu multiligne dans un fichier `tee` :
``` bash
tee -a mon_fichier.txt <<EOF
com contenu 1
com contenu 2
EOF
```
- `EOF` correspond à End Of File. c'ets une convention. N-importe quelle chaine "unique" peut être utilisée
- l'option `-a` permet d'ajouter du contenu. Sans cette option le contenu est remplacé.


---
## Syntaxe de scripts

Continuer la commande sur une autre ligne : `\`. attention ne pas mettre d'espace après

Enchaîner le lancement d'une  une commande après une autre : `&&`



---
## Se connecter à d'autre systèmes

Faire une requête vers un site internet ou une API `curl URL`

Tester l'accès à un système distant : `ping NOM_OU_ADRESSE_IP` (attention, le ping peut être interdit sur le système distant)

Se connecter en ssh : `ssh USER@NOM_OU_ADRESSE_IP`


---
## Installer des application dans un système Ubuntu

Mettre à jour la liste des dépôt : `apt-get update`

Recherche un package : `apt-cache search  NOM_DU_PACKAGE`

Installer un package : `apt-get install -y NOM_DU_PACKAGE`

- L'option `-y` permet d'installer le package sans confirmation
  
---
## Installer des application dans un système Alpine

Mettre à jour la liste des dépôt : `apk update`

Recherche un package : `apk search NOM_DU_PACKAGE`

Installer un package : `apk add  -y NOM_DU_PACKAGE`


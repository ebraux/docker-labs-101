## Docker attach

La commande `docker attach <CONTAINER NAME or ID>`  se connecte directement au terminal du processus principal du conteneur, contrairement à la l'utilisation de `exec` avec l'option `-t`, qui elle lance un nouveau terminal.

Elle permet d'interagir directement avec processus qui a été lancé.

`attach` utilise STDIN et STDOUT. Son fonctionnement est donc limité par ce qui est permis par le processus lancé dans le terminal principal :

- Certains processus ignorent les entrées sur STDIN :
    - Il est parfois impossible d'interagir avec le processus
    - Dans certains cas, la séquence CTRL+p puis CTRL+q ne fonctionne pas
    - Dans la plupart des cas, CTRL+C et CTRL+D fonctionnent
- Certains processus n'affichent rien sur STDOUT

Elle est principalement utilisée pour des taches de débogage, et impose que le processus lancé affiche des informations sur STDOUT.

Il est possible de désactiver STDIN, avec l'option `--no-stdin`, pour éviter les fausses manipulations, et l'arrêt du conteneur.


---
## Lancement d'un conteneur en mode détaché
Pour tester la connexion, lancer un conteneur en mode interactif :
``` bash
docker run -it --rm --name cont1 alpine
# /#
```

Dans le conteneur, lancer une commande qui affiche un message en boucle.
``` bash
while true; do sleep 1; echo "Hello"; done
# Hello
# Hello
# ...
```

Pour les autres opérations, ouvrir une autre fenêtre de terminal

---
## Se connecter avec `exec`

Se connecter en mode interactif avec `exec` :
``` bash
docker exec -it cont1 sh
```
Aucun message ne s'affiche, la commande s'execute dans un autre terminal, que celui lancé lors du run.

Sortir de la session avec la commande `exit`.

Dans le terminal utilisé pour lancer le conteneur, on constate qu'il est toujours actif. La commande `exit`  s'est appliquée uniquement à la session lancée  avec `exec`.

---
## Se connecter avec `attach`


### Connexion standard

``` bash
docker attach cont1
# Hello
# Hello
# ...
```
La commande `attach` ne lance pas de nouveau terminal, mais se connecte directement au terminal du processus principal du conteneur. Ici, ce terminal exécute la commande qui affiche "Hello" en boucle.

La commande `attach` prend par défaut en compte STDOUT (affichage de "Hello"), mais également STDIN. Si on lance une commande, elle sera prise en compte.

- Saisir des caractères dans le terminal attaché : ils s'affichent dans le terminal initial
- Saisir CTRL+C : la commande d'affichage de "Hello" s'arrête dans le terminal initial
- Saisir la commande `exit`, ou CTRL+D : met fin au shell, le conteneur s'arrête.

Pour détacher une session `attach`, utiliser la séquence CTRL+p puis CTRL+q :

- Le message `/ # read escape sequence` s'affiche,
- Le terminal se détache
- Le conteneur continue à tourner dans le terminal initial

### Connexion sans STDIN

Ne pas activer STDIN permet de limiter les risques de mauvaise manipulation.

Se connecter avec `attach --no-stdin` :
``` bash
docker attach --no-stdin cont1
# Hello
# Hello
# ...
``` 

- Saisir des caractères dans le terminal attaché : il ne sont pas pris en compte dans le terminal initial.
- Saisir CTRL+C : la commande `docker attach` s'arrête dans son terminal. L''affichage de "Hello" continue dans le terminal initial
- Saisir la commande `exit`, ou CTRL+D : ca n'a aucun effet.
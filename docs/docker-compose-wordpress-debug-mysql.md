# Remise en service d'une base Mysql

---
## Contexte

Suite à une mauvaise gestion du tag :

- La base qui devait être en v5.7 a été lancée en version 8.
- Les données ont été converties en version 8 :
    -  Message `Update Modle 5070 to 8000` dans les logs.
- Le retour en version 5.7 n'a pas fonctionné : 
    - Message `InnoDB: corruption in the InnoDB tablespace.`
- La relance en version 8 provoque une erreur (voir message complet ci dessous)

Le serveur Mysql ne démarre donc plus.

---
## Message d'erreur

```bash
---
db_1         | 2024-02-22T11:28:34.483734Z 1 [ERROR] [MY-012526] [InnoDB] Upgrade is not supported after a crash or shutdown with innodb_fast_shutdown = 2. This redo log was created with MySQL 5.7.44, and it appears logically non empty. Please follow the instructions at http://dev.mysql.com/doc/refman/8.3/en/upgrading.html
db_1         | 2024-02-22T11:28:34.483749Z 1 [ERROR] [MY-012930] [InnoDB] Plugin initialization aborted with error Generic error.
db_1         | 2024-02-22T11:28:34.944815Z 1 [ERROR] [MY-010334] [Server] Failed to initialize DD Storage Engine
db_1         | 2024-02-22T11:28:34.945202Z 0 [ERROR] [MY-010020] [Server] Data Dictionary initialization failed.
db_1         | 2024-02-22T11:28:34.945220Z 0 [ERROR] [MY-010119] [Server] Aborting
db_1         | 2024-02-22T11:28:34.946278Z 0 [System] [MY-010910] [Server] /usr/sbin/mysqld: Shutdown complete (mysqld 8.3.0)  MySQL Community Server - GPL.
db_1         | 2024-02-22T11:28:34.946288Z 0 [System] [MY-015016] [Server] MySQL Server - end.
```

Quelques recherches sur Google, indiquent que :

- La plupart des solutions proposées dans les forums consistent à supprimer les données, et re-injecter un backup.
- La désactivation des redo-logs en v8 ne peut se faire que via des commandes SQL
- Une des options serait de lancer la base, en mode recovery, avec l'option `innodb_force_recovery = 1`
    - [https://dev.mysql.com/doc/refman/8.0/en/forcing-innodb-recovery.html](https://dev.mysql.com/doc/refman/8.0/en/forcing-innodb-recovery.html), 


Dans la documentation d'upgrade Mysql, en suivant le lien indiqué dans log d'erreur, on trouve des informations sur les redo-log : [https://dev.mysql.com/doc/refman/8.3/en/upgrade-prerequisites.html](https://dev.mysql.com/doc/refman/8.3/en/upgrade-prerequisites.html)
```
If upgrade to MySQL 8.3 fails due to any of the issues outlined above, the server reverts all changes to the data directory.
In this case, remove all redo log files and restart the MySQL 8.2 server on the existing data directory to address the errors.
The redo log files (ib_logfile*) reside in the MySQL data directory by default.
After the errors are fixed, perform a slow shutdown (by setting innodb_fast_shutdown=0) before attempting the upgrade again.
```

Il faudrait donc lancer la base en mode recovery, en configurant :
``` ini
[mysqld]
innodb_force_recovery = 1
innodb_fast_shutdown = 0
```

---
## Application de la procédure

### Lancement de Mysql en mode recovery

Dans la documentation de l'image sur le dockerhub [https://hub.docker.com/_/mysql](https://hub.docker.com/_/mysql), la rubrique "Using a custom MySQL configuration file" indique d'utiliser l'option ` -v /my/custom:/etc/mysql/conf.d` pour ajouter un fichier custom à Mysql.

On utilise un conteneur :

- A partir de l'image mysql utilisée ans la stack
- En montant le volume de base de données créé dans la stack compose.

Attention, le volume est un volume interne à la stack. Il faut donc utiliser `wp_db` et pas uniquement `db` comme dans le fichier docker-compose.yml.
``` bash
docker volume list
# ...
# wp_db
# ...
```

Lancement de Mysql :
``` bash
docker run --rm \
  --name mysql-recov \
  -v wp_db:/var/lib/mysql \
  -v ./innodb-recovery.cnf:/etc/mysql/conf.d/innodb-recovery.cnf \
   mysql
```

Le message d'erreur a évolué :
``` bash
[InnoDB] Cannot upgrade format (v1) of redo log files when innodb-force-recovery > 0.
```
Il faut donc bien supprimer les fichiers de redo log.

> Supprimer ces fichiers peu avoir des conséquences sur les mécanisme de sauvegarde ou de clustering qui utilisent les redolog.



### Suppression des fichiers de redolog


Re-lancer le conteneur mysql-recov, mais lancer un shell interactif au lieu de mysql :
``` bash
docker run --rm \
  -it \
  --name mysql-recov \
  -v wp_db:/var/lib/mysql \
  mysql sh
#sh-4.4#
``` 

Et dans le conteneur, supprimer les fichiers de redo
``` bash
cd /var/lib/mysql
mkdir backup_redo
mv ib_logfile* backup_redo
exit
```

> En version 5.x, les fichiers de redo logs sont des fichiers `ib_logfile*`. En version 8, ils sont désormais dans le fichier `#innodb_redo`.

### Relancer mysql en mode recovery

Lancement de Mysql :
``` bash
docker run --rm \
  --name mysql-recov \
  -v wp_db:/var/lib/mysql \
  -v ./innodb-recovery.cnf:/etc/mysql/conf.d/innodb-recovery.cnf \
   mysql
```

Cette fois le serveur démarre correctement.

Arrêter mysql :
``` bash
docker stop mysql-recov
```

---
## Relancer la stack

``` bash
docker compose up
```

La stack démarre sans erreurs.

Le site wordpress est de nouveau accessible.


---
## Corriger la template

Pour éviter ce type de problème à l'avenir :

- Identifier sur le dockerhub à quelle version de mysql correspond le tag latest : `8.3.0, 8.3, 8, innovation, latest,  ...`
- Spécifier cette version dans le fichier docker-compose :
``` yaml
  db:
    image: mysql:8.3
```
- Relancer la stack


---
## Informations complémentaires sur la configuration de Mysql

Lancer un conteneur mysql-custom à partir de limage mysql :
``` bash
docker run -it --rm --name mysql-custom mysql sh
#sh-4.4#
``` 

Dans le conteneur, vérifier la configuration
``` bash
ls /etc/mysql/*
```

Aucun fichier `my.cnf` comme indiqué dans la documentation. Il faut recherche ce fichier :
``` bash
find /etc -name my.cnf
# /etc/my.cnf
```
Le fichier de configuration de mysql est donc directement placé dans `/etc`, et non dans `/etc/mysql`.

Vérification du dossier d'inclusion des configurations :
``` bash
sh-4.4# cat /etc/my.cnf | grep includedir 
# !includedir /etc/mysql/conf.d/
```

Par contre là c'est bon, le dossier pour inclure des configurations personnalisées est bien /etc/mysql/conf.d/
``` bash
ls /etc/my*
# /etc/my.cnf

# /etc/my.cnf.d:

# /etc/mysql:
# conf.d
```
Sortir du conteneur
``` bash
exit
```


Lancer un conteneur mysql pour initialiser une base de donnés
``` bash
docker run --rm \
  --name mysql-config \
  -d \
  -e MYSQL_ROOT_PASSWORD='secret' \
  -v db-config:/var/lib/mysql \
   mysql
```

Afficher les variables à modifier : 
``` bash
docker exec -it  mysql-config mysql -psecret -e "SHOW VARIABLES LIKE '%innodb_force_recovery%';"
# +-----------------------+-------+
# | Variable_name         | Value |
# +-----------------------+-------+
# | innodb_force_recovery | 0     |
# +-----------------------+-------+

docker exec -it  mysql-config mysql -psecret -e "SHOW VARIABLES LIKE '%innodb_fast_shutdown%';"

# +-----------------------+-------+
# | Variable_name         | Value |
# +-----------------------+-------+
# | innodb_fast_shutdown  | 1     |
# +-----------------------+-------+
```

Arrêter le conteneur
``` bash
docker stop  mysql-config
```


Créer le fichier de customisation
``` bash
tee innodb-recovery.cnf <<EOF
[mysqld]
innodb_force_recovery = 1
innodb_fast_shutdown = 0
EOF
```


Lancer mysql, avec cette configuration
``` bash
docker run --rm \
  --name mysql-config \
  -d \
  -v db-config:/var/lib/mysql \
  -v ./innodb-recovery.cnf:/etc/mysql/conf.d/innodb-recovery.cnf \
   mysql
```

Et vérifier à nouveau les variables :

-  `innodb_force_recovery` est bien passé à `1`
-  `innodb_fast_shutdown` est bien passé à `0`
 

Arrêter le conteneur
``` bash
docker stop  mysql-config
docker volume rm db-config
```








# Manipulation des images


## Objectifs

* Comprendre les concepts de conteneur, d'image, et de registry.

## Lancement du conteneur "Hello World" 

Dans le lab [Lancement d'un conteneur](container_run.md), le lancement du conteneur à déclenché le téléchargement (pull) de l'image "hello-world" depuis la "registry".

Lancement du conteneur :
``` bash
docker run --rm --name my-hello  hello-world
```

Affichage des images : 
```bash
docker image ls
# REPOSITORY   TAG     IMAGE ID      CREATED       SIZE
# hello-world  latest  bf756fb1ae65  4 months ago  13.3kB
```


## Supprimer une image

Afficher la liste des images :

```bash
docker image ls
```

Supprimer l'image hello-world :

```bash
docker image  rm   hello-world
```

Vérifier que l'image a bien été supprimée avec `docker image ls`.

## Pré-charger une image

Télécharger l'image hello-world depuis la "Docker Registry"

```bash
docker image pull hello-world
# Using default tag: latest
# latest: Pulling from library/hello-world
# b8dfde127a29: Pull complete 
# Digest: sha256:5122f6204b6a3596e048758cabba3c46b1c937a46b5be6225b835d091b90e46c
# Status: Downloaded newer image for hello-world:latest
# docker.io/library/hello-world:latest
```

Vérifier qu'elle est bien présente en local :

```bash
docker image ls
```

## Lancement d'un conteneur avec une image préchargé

Vérifier que si on lance le conteneur "hello", c'est bien l'image locale qui est utilisée.

Commande :

``` bash
docker run --rm --name my-hello  hello-world
```
Résultat : l'execution du conteneur est plus rapide puisque l'image est déjà disponible en local.


## Interprétation du nom de l'image

On remarque dans la commande pull, que Docker va compléter le nom de l'image : 

- Le format d'une image est `<registry>/<path>:<tag>
- Docker utilise les valeurs par défaut
  - registry = docker.io
  - version = latest.
  - Docker va d'abord chercher les images "officielle", contenues dans le dossier "library"

L'image affichée "hello-world", correspond donc à "docker.io/library/hello-world:latest"

Le chargement de l'image avec le nom complet ne provoque pas le chargement d'une nouvelle image :

```bash
docker image pull docker.io/library/hello-world:latest
docker image ls
```

## Ménage 

Suppression de l'image "hello-world"

```bash
docker image rm hello-world
```

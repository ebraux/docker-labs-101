# Lab Docker Volumes et persistance des données

---
## Objectif

Utilisation d'une image Nginx pour illustrer l'utilisation des  volumes Docker pour assurer la persistance des données.

Cas d'usage : publication de contenu livré en production

---
## Principe

Les "volumes Docker" permettent également d'accéder à des données stockées en dehors du conteneur.

Les données peuvent être stockées sur la machine hôte, mais également sur d'autres type de stockage distant (NFS, cloud, ...)

Comme pour les volumes montés, l'option `-v` est utilisée.

Cette option permet également de nommer les volumes, pour faciliter leur utilisation.

Un conteneur Nginx sera utilisé pour publier le contenu, en lecture seul pour améliorer la sécurité du site

Un conteneur Alpine sera utilisé pour accéder aux données en lecture écriture et les modifier.

---
## Lancement du conteneur attaché à un volume

Vérifier l’existence de volume (ou non) :
``` bash
docker volume ls
# DRIVER    VOLUME NAME
```
Si des volumes sont présents, mémoriser la liste des volume.


Lancer un conteneur alpine, et utiliser l'option `-v <POINT_DE_MONTAGE>` pour demander la création d'un volume.
``` bash
docker run -t --rm --name c-write -d -v /datas alpine
```

Un volume a été créé :
``` bash
docker volume ls
# DRIVER    VOLUME NAME
# local     0acb68815147610541946906826640171882628a3b5c2c53954859c46790295b
```

Par défaut docker donne un ID comme nom de volume. 2 inconvénients :

- Difficilement reconnaissable si plusieurs volumes sont créés
- A chaque lancement de la commande, un nouveau volume est créé

---
## Test de suppression du conteneur

Supprimer et recréer le container
``` bash
docker stop c-write
docker run  -t --rm --name c-write -d -v /datas alpine
```
Observer la liste des volumes
``` bash
docker volume ls
# DRIVER    VOLUME NAME
# local     0acb68815147610541946906826640171882628a3b5c2c53954859c46790295b
# local     46fb4405719d063193511e9617b888bedf83a5e0a2879a187cb57857c1bbf104
```

---
## Utilisation d'un volume "nommé"

Pour éviter ces problèmes, il est recommandé de créer un volume avec un nom :

``` bash
docker volume create volume_my_site

docker volume ls
# DRIVER    VOLUME NAME
# local     0acb68815147610541946906826640171882628a3b5c2c53954859c46790295b
# local     46fb4405719d063193511e9617b888bedf83a5e0a2879a187cb57857c1bbf104
# local     volume_my_site
```

Puis de spécifier le nom du volume a attacher au conteneur, avec l'option `-v <VOLUME_NAME>:<POINT_DE_MONTAGE>`

``` bash
docker stop c-write
docker run  -t --rm --name c-write -d -v volume_my_site:/datas alpine
```

> Si vous utilisez la syntaxe `-v volume_my_site:/data` et que le volume n'existe pas, Docker va le créer, avec les options par défaut.

``` bash
docker stop c-write
```

Le volume est volume toujours présent car externe. Il a sont propre cycle de vie.

---
## Créer du contenu dans le volume, et vérifier la persistance


Lancer un conteneur avec accès en écriture
``` bash
docker run  -t --rm --name c-write -d -v volume_my_site:/datas alpine 
```

Pour l'instant, le volume ne contient aucune données

``` bash
docker exec c-write ls -l /datas
```

Comme c'est un objet Docker, c'est Docker qui le gère complètement. Pour ajouter des données, il faut donc passer par Docker, et, par exemple, se connecter au conteneur.

Connexion au conteneur
``` bash
docker exec -it c-write /bin/sh
```

Création de contenu :
``` bash
ls /datas

tee  /datas/index.html <<EOF
<html>
  <body>
    <h1>My Site</h1>
  </body>
</html>
EOF

cat /datas/index.html

exit
```

Vérification du fichier créé : 

``` bash
docker exec c-write cat /datas/index.html
```

Pour valider la persistance des données:

- Supprimer le conteneur

``` bash
docker stop c-write
```
- En relancer un nouveau attaché au volume, et vérifier le contenu de "/datas"
``` bash
docker run  -t --rm --name c-write -d -v volume_my_site:/datas alpine
docker exec c-write cat /datas/index.html
```

Le contenu du fichier est toujours présent, malgré la suppression du conteneur. Les données sont persistantes.

Arrêter le conteneur

``` bash
docker stop c-write
```

---
## Publier les données avec Nginx

Lancer un conteneur nginx et connecter le volume en mode lecture seule dans le dossier de publication par défaut `/usr/share/nginx/html`.

``` bash
docker run  --rm --name c-publish -d -p 8081:80 -v volume_my_site:/usr/share/nginx/html:ro nginx
```

Et accès au site via un navigateur : [http://localhost:8081](http://localhost:8081)

Ou avec curl
``` bash
curl http://localhost:8081
```

Une page contenant le message "My Site" doit s'afficher.


Le volume `volume_my_site` a été monté indifféremment :

- Dans le dossier "/datas" dans le conteneur c-write
- Dans le dossier "/usr/share/nginx/html" dans le conteneur c-publish


Essayer de modifier les données

Connexion au conteneur
``` bash
docker exec c-publish touch /usr/share/nginx/html
# touch: setting times of '/usr/share/nginx/html': Read-only file system
```

Ce n'est pas possible, car le volume a été monté en lecture seule.


---
## Modifier les données grâce au conteneur en écriture


Lancer le conteneur alpine c-write en mode shell interactif
``` bash
docker run  -it --rm --name c-write -v volume_my_site:/datas alpine
```

Modifier le contenu du volume, et sortir du conteneur :
``` bash
tee  /datas/index.html <<EOF
<html>
  <body>
    <h1>My Site !!! </h1>
  </body>
</html>
EOF

exit
```

Accéder au site. Une page contenant le message "My Site !!!" doit s'afficher.


---
## Obtenir des informations sur le volume

Pour obtenir plus d'informations sur un volume :

``` bash
docker inspect volume_my_site
```
``` json
[
    {
        "CreatedAt": "2021-11-15T12:27:01+01:00",
        "Driver": "local",
        "Labels": {},
        "Mountpoint": "/var/lib/docker/volumes/volume_my_site/_data",
        "Name": "volume_my_site",
        "Options": {},
        "Scope": "local"
    }
]
```

Attention, il ne faut pas modifier directement le contenu d'un volume, au risque de le corrompre. Son accès est d'ailleurs protégé.

Vérifier le contenu du fichier "test.txt"
``` bash
sudo ls  /var/lib/docker/volumes/volume_my_site/_data
sudo cat /var/lib/docker/volumes/volume_my_site/_data/test.txt
``` 

---
## Ménage

``` bash
docker stop c-publish
docker volume rm volume_my_site
```

Et suppression des volumes non nommés crées
``` bash
docker volume ls
docker volume rm VOLUME_NAME_1 VOLUME_NAME_2
```
ou suppression de tous les volumes non utilisés :
``` bash
docker volume prune
```

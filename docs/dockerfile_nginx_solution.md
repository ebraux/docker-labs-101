# Personnalisation d'une image : Serveur Nginx

Proposition de solution pour le lab [DockerFile Serveur Nginx](./dockerfile_nginx.md)


---
## Création du Dockerfile

Créer un dossier "my_nginx" et y créer un fichier Dockerfile contenant:

- Le nom de l'image source : mot clé `FROM`
- L'installation des paquets : mot clé `RUN`
- La configuration de la commande à exécuter : mot clé `CMD`

> Informations sur les commandes qui peuvent être utilisées dans un Dockerfile : [https://docs.docker.com/engine/reference/builder/](https://docs.docker.com/engine/reference/builder/)

- Création du dossier du lab
```bash
mkdir my_nginx; cd my_nginx
```

- Création du Dockerfile

=== "Dockerfile"

    ``` bash
    FROM debian:bullseye-slim

    RUN set -x \
        && echo "Mise a jour de la liste des paquets" \
        && apt-get update \
        && echo "Installation" \
        && apt-get install -y nginx \
        && echo "Ménage" \
        && apt-get remove --purge --auto-remove -y \
        && rm -rf /var/lib/apt/lists/*

    EXPOSE 80

    CMD ["/usr/sbin/nginx", "-g", "daemon off;"]
    ```
  
=== "Create Dockerfile with tee" 

    ``` bash
    tee Dockerfile <<EOF
    FROM debian:bullseye-slim

    RUN set -x \\
        && echo "Mise a jour de la liste des paquets" \\
        && apt-get update \\
        && echo "Installation" \\
        && apt-get install -y nginx \\
        && echo "Ménage" \\
        && apt-get remove --purge --auto-remove -y \\
        && rm -rf /var/lib/apt/lists/*

    EXPOSE 80

    CMD ["/usr/sbin/nginx", "-g", "daemon off;"]

    EOF
    ```

- Vérification
```bash
cat Dockerfile
```

!!! warning "Implémentation de la commande `/usr/sbin/nginx -g 'daemon off;'`"

    La partie `'daemon off;'` est une directive de configuration pour Nginx, associée à l'option `-g`. Elle contient des espaces, et doit être placée entre guillemets `'`, pour être interprétée comme une chaîne quand elle est utilisée en ligne de commande. Par contre quand on utilise le format "JSON arguments", chaque élément est considéré comme une chaîne, il ne faut donc pas utiliser de guillemets. Ce qui donne les 2 solutions possibles pour la ligne `CMD`:

    - `CMD ["/usr/sbin/nginx", "-g", "daemon off;"]`
    - `CMD /usr/sbin/nginx -g 'daemon off;'`

!!! tip

    `set -x` active le mode de débogage : le shell va afficher chaque commande exécutée, précédée d'un signe +, avant de l'exécuter. 

---
##  Builder l'image

Le build de l'image se fait en 3 étapes : 

- Étape 1 : récupération de l'image de base "debian:bullseye-slim"
- Étape 2 : Installation de nginx
- Étape 3 : configuration de la commande par défaut du conteneur,
- Étape 4 : construction de la une nouvelle image, avec le nom "my_nginx:latest"

```bash
docker build -t  my_nginx .	   
# [+] Building 16.9s (6/6) FINISHED                                                                            
#  => [internal] load build definition from Dockerfile                                  0.0s
#  => => transferring dockerfile: 370B                                                  0.0s
#  => [internal] load .dockerignore                                                     0.0s
#  => => transferring context: 2B                                                       0.0s
#  => [internal] load metadata for docker.io/library/debian:bullseye-slim               0.9s
#  => CACHED [1/2] FROM docker.io/library/debian:bullseye-slim@sha256:7606bef5684b ...  0.0s
#  => [2/2] RUN set -x  && echo "Mise a jour de la liste des paquets"  && apt-get      15.6s
#  => exporting to image                                                                0.4s
#  => => exporting layers                                                               0.3s
#  => => writing image sha256:45ca784d5839 ... cce9e6255a36aa81dc                       0.0s 
#  => => naming to docker.io/library/my_nginx                                           0.0s 
```

- L'option `-t` sert à nommer l'image
- Le point à la fin de la ligne indique qu'on travaille dans le dossier courant


Affichage des images
```bash
docker image ls
# REPOSITORY    TAG       IMAGE ID       CREATED          SIZE
# my_nginx      latest    45ca784d5839   28 seconds ago   145MB
```

On retrouve l'image "my_nginx".


---
## Lancement d'un conteneur pour test

Lancement d'un conteneur avec l'image "mysite" personnalisée :
``` bash
docker run -d --rm --name c-my-nginx  -p 8081:80 my_nginx
```

Et accès au site via un navigateur : [http://localhost:8081](http://localhost:8081)

Ou avec curl
``` bash
curl http://localhost:8081
```

Une page contenant le message "Welcome to nginx!" doit s'afficher.

---
## Ménage


Suppression du dossier :
```bash
cd ..
rm -rf my_nginx
```
Suppression du conteneur:
```bash
docker stop c-my-nginx
```

Suppression de l'image : 
```bash
docker rmi mysite
```


# Premiers pas avec Docker

Quelques exemples d'utilisation.

L'objectif est de se rendre de compte de ce qu'il est possible de faire.

Les autres labs permettrons de "comprendre".

---
## Vérification

Tout d'abord, vérifier que Docker est installé. 
```bash
docker --version
# Docker version xx.xx.xx, build xxxxxxxxxx
```

- Si ce n'est pas le cas [installer Docker](https://ebraux.gitlab.io/docker-labs/)


---

## Le classique "Hello world"

```bash
docker run --rm  hello-world
```

Cette commande :

- Lance un conteneur à partir de l'image "hello-world"
- Supprime le conteneur après execution



---
## Lancer un serveur web

Un serveur web permet d'afficher via un navigateur une page au format HTML. Le serveur web le plus répandu est Apache HTTPD

Lancer un serveur Apache Httpd : 
```bash
docker run --rm  -p 8081:80 httpd
```

Cette commande :

- Lance un conteneur à partir de l'image "httpd"  
- Le rend accessible par le port 8081 de la machine hôte
- Supprime le conteneur après execution


> Httpd étant un serveur, Docker ne nous rend pas la main.


Puis :

- Se connecter avec un navigateur à l'adresse [http://localhost:8081](http://localhost:8081)
- Le message "It works!" doit s'afficher.
- Les requêtes de connexion sont visibles dans le terminal.

> Pour arrêter le serveur (et donc le conteneur), tapper ++ctrl+c++ dans le terminal.


---
## Générer un mot de passe au format "htpasswd"

Le serveur WEB Apache propose une gestion d'accès par login mot de passe utilisant un mécanisme de chiffrement de données: [https://httpd.apache.org/docs/2.4/fr/programs/htpasswd.html](https://httpd.apache.org/docs/2.4/fr/programs/htpasswd.html).

Ce format a été repris par d'autres outils, et correspond au mode "Basic authentication".

Il arrive donc parfois de devoir générer un mot de passe au format "htpasswd", alors qu'Apache n'est pas installé sur la machine.

Avant Docker, il fallait installer le paquet Apache.

Avec Docker, il suffit de lancer le container Apache, et d'executer la commande :
```bash
docker run --rm httpd htpasswd -nb user1 password1
# user1:$apr1$s.CUKaGv$0hGZjNdb9So2V.nG0s/rF1
```
Cette commande :

- Lance un container à partir de l'image "httpd"
- Lui fait exécuter la commande `htpasswd -nb user1 password1`
- Supprime le container après execution


---
## Ménage

Suppression des images "hello-world" et "httpd" de la registry locale 
```bash
docker rmi httpd hello-world
```

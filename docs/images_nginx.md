# Observer la gestion des tags et des images

---
## Parcourir le Docker Hub
Lister les image correspondant à Nginx dans le Docker Hub

```bash
docker search nginx
# NAME                              DESCRIPTION                   STARS  OFFICIAL  AUTOMATED
# nginx                             Official build of Nginx.      18538  [OK]       
# unit                              Official build of NGINX  ...  3      [OK]       
# bitnami/nginx                     Bitnami nginx Docker Ima ...  164              [OK]
# bitnami/nginx-ingress-controller  Bitnami Docker Image for ...  29               [OK]
# ubuntu/nginx                      Nginx, a high-performanc ...  91                   
# kasmweb/nginx                     An Nginx image based off ...  6                    
# rancher/nginx-ingress-controller                                11                   
# bitnami/nginx-exporter                                          3                    
# rancher/nginx                                                   2                    
# rapidfort/nginx-ib                RapidFort optimized, ha ...   10                   
# rapidfort/nginx                   RapidFort optimized, ha ...   14                   
# vmware/nginx-photon                                             1                    
# bitnami/nginx-ldap-auth-daemon                                  3                    
# rapidfort/nginx-official          RapidFort optimized, ha ...   10                   
# vmware/nginx                                                    2                    
# rancher/nginx-conf                                              0                    
# linuxserver/nginx                 An Nginx container, bro ...   201                  
# privatebin/nginx-fpm-alpine       PrivateBin running on a ...   72               [OK]
# bitnami/nginx-intel                                             1                    
# bitnamicharts/nginx                                             0                    
# rancher/nginx-ssl                                               0                    
# circleci/nginx                    This image is for inter ...   2                    
# redash/nginx                      Pre-configured nginx to ...   2                    
# continuumio/nginx-ingress-ws                                    0           
```

---
## Récupérer un image Nginx

Lister les images sur votre machine :
```bash
docker images
# REPOSITORY                  TAG       IMAGE ID       CREATED        SIZE
# hello-world                 latest    9c7a54a9a43c   2 weeks ago    13.3kB
```

Faire un pull de l'image `nginx:latest`
```bash
docker pull nginx
# Using default tag: latest
# latest: Pulling from library/nginx
# 9e3ea8720c6d: Already exists 
# bf36b6466679: Pull complete 
# 15a97cf85bb8: Pull complete 
# 9c2d6be5a61d: Pull complete 
# 6b7e4a5c7c7a: Pull complete 
# 8db4caa19df8: Pull complete 
# Digest: sha256:b27af4fc28c99060ed5bae7f4b7c913611e1ae2a6f63b6c0325d668b4cf26a61
# Status: Downloaded newer image for nginx:latest
# docker.io/library/nginx:latest
```

Lister les images :
```bash
docker images
# REPOSITORY                  TAG       IMAGE ID       CREATED        SIZE
# hello-world                 latest    9c7a54a9a43c   2 weeks ago    13.3kB
# nginx                       latest    448a08f1d2f9   2 weeks ago    142MB
```

L'image Nginx est disponible, avec le Tag `latest`.

---
## Récupérer une image avec un tag alternatif à "latest"

Consulter la page de l'image Nginx sur le Docker Hub [https://hub.docker.com/_/nginx](https://hub.docker.com/_/nginx), et faire un pull d'une version spécifique de l'image Nginx, qui partage le tag latest :

Exemple au 22/05/2023 : `1.23.4, mainline, 1, 1.23, latest, 1.23.4-bullseye, mainline-bullseye, 1-bullseye, 1.23-bullseye, bullseye`

```bash
docker pull nginx:1.23.4
# 1.23.4: Pulling from library/nginx
# Digest: sha256:3686e1f714d2fd238f1624b14f8eece48677d4471aeaededb69420086c9ff186
# Status: Downloaded newer image for nginx:1.23.4
# docker.io/library/nginx:1.23.4
```

Lister les images :

```bash
docker images
# REPOSITORY                  TAG       IMAGE ID       CREATED        SIZE
# hello-world                 latest    9c7a54a9a43c   2 weeks ago    13.3kB
# nginx                       1.23.4    448a08f1d2f9   2 weeks ago    142MB
# nginx                       latest    448a08f1d2f9   2 weeks ago    142MB
```

L'image Nginx est disponible, avec le Tag `latest` et le tag `1.23.4`.

L'image ID est identique, ce sont les même images, avec des tags différents.

---
## Récupérer une image dans une version différente

Consulter la page de l'image Nginx sur le Docker Hub [https://hub.docker.com/_/nginx](https://hub.docker.com/_/nginx), et faire un pull d'une version spécifique de l'image Nginx, qui ne partage PAS le tag `latest`, mais plutôt le tag `stable` :

Exemple au 22/05/2023 : `1.24.0, stable, 1.24, 1.24.0-bullseye, stable-bullseye, 1.24-bullseye`

```bash
docker pull nginx:1.24.0
# 1.24.0: Pulling from library/nginx
# 9e3ea8720c6d: Already exists 
# ee7feb8b89d4: Pull complete 
# 3726de4affbb: Pull complete 
# 4b5188c33a72: Pull complete 
# 3c90f9dd7b85: Pull complete 
# 2246bda193a8: Pull complete 
# Digest: sha256:4e6ce3aec028b0477f570ff77810a137ffa3d32f392262f07f1cc011c7659e7b
# Status: Downloaded newer image for nginx:1.24.0
# docker.io/library/nginx:1.24.0
```

```bash
docker images
# REPOSITORY                  TAG       IMAGE ID       CREATED        SIZE
# hello-world                 latest    9c7a54a9a43c   2 weeks ago    13.3kB
# nginx                       1.24.0    8409c6410d36   2 weeks ago    142MB
# nginx                       1.23.4    448a08f1d2f9   2 weeks ago    142MB
# nginx                       latest    448a08f1d2f9   2 weeks ago    142MB
```

L'image Nginx est disponible, avec les Tag `latest`, `1.23.4` et `1.24.0`.

L'image ID est identique pour les tags `latest` et `1.23.4`, ce sont les même images, avec des tags différents.

Mais l'image ID est différent pour le tag `8409c6410d36`. C'est une image différente.


!!! note "Remarque :" 

    - Intuitivement une image 'stable' devrait être la dernière version mineur de la dernière version majeure, donc la 1.23.4
    - Et une image 'latest' devrait être la toute dernière version, et donc la 1.24.0
    - C'est le contraire dans le cas présent. c'est au mainteneur du repository de définir quelle images sont taguées en "latest".



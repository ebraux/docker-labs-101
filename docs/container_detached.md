# Lancement d'un conteneur de type serveur (mode détaché)

---
## Objectifs

* Lancer un conteneur pour qu'il fournisse un service.
* Accéder à ce service depuis le poste local.

---
## Lancement d'un conteneur en mode détaché"

On utilise l'option `-d` pour lancer le conteneur en mode détaché. Le conteneur est alors détaché du shell courant. On récupère la main après l'avoir lancé.

Si la commande lancée est prévue pour ne jamais s'arrêter (`sleep infinity` par exemple), le conteneur tourne en arrière plan. C'est l'équivalent d'un mode "daemon".

```bash
docker run --name cont1 -d alpine sleep infinity
```
    
On peut observer avec `docker ps` que le conteneur reste en tache de fond, en status UP.

Arrêt du conteneur :
```bash
docker stop cont1
```

Avec la commande `docker ps -a`, on voit que le conteneur a été lancé, est resté actif tant qu'on était connecté, et s'est arrêté quand la commande `exit` a été saisie ( durée entre `CREATED` et `Exited` )

Relance du conteneur :
```bash
docker start cont1
```

Avec la commande `docker ps`, on voit que le conteneur est de nouveau actif.

---
## Ménage

``` bash
docker stop cont1
docker rm cont1
```


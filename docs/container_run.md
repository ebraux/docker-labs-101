# Lancement d'un conteneur

---
## Objectifs

* Lancer un conteneur.
* Comprendre les concepts de conteneur d'image et de registry.

---
## Lancement d'un conteneur "Hello World"


Avant de lancer un conteneur lister les éléments présents  sur le système :

``` bash
docker ps
```
> Il est également possible d'utiliser  `docker container ls`

``` bash
docker image ls
```

Vérifier qu'aucun conteneur ou image ne correspondent à "Hello World".


### Lancer le conteneur
Commande :

``` bash
docker run hello-world
# Unable to find image 'hello-world:latest' locally
# latest: Pulling from library/hello-world
# 1b930d010525: Pull complete
# Digest: sha256:6f744a2005b12a704d2608d8070a494ad1145636eeb74a570c56b94d94ccdbfc
# Status: Downloaded newer image for hello-world:latest

# Hello from Docker!
# This message shows that your installation appears to be working correctly.
```

Déroulement :

1. Le Client Docker se connecte au daemon Docker
2. Le daemon Docker recherche l'image "hello-world" sur le système local
3. Pas d'image locale : le deamon docker télécharge (pull) l'image depuis la "registry" (Docker Hub) et l'ajoute en local
4. Le daemon Docker lance un conteneur à partir de cette image, qui exécute une commande (RUN) qui affiche le message sur la sortie standard du conteneur
5. Le daemon Docker envoie la sortie standard vers le Client Docker, qui l'affiche sur le terminal.


### Lister les images

``` bash
docker image ls 
# REPOSITORY    TAG      IMAGE ID        CREATED             SIZE
# hello-world   latest   bf756fb1ae65    9 months ago        13.3kB
```
L'image est maintenant présente sur le système local.

### Lister les conteneurs

``` bash
docker ps
```

Aucun conteneur... Où est "hello-world" ?

Utilisation de l'option `-a` pour lister même les conteneurs non actif:

``` bash
docker ps -a
# CONTAINER ID  IMAGE        COMMAND   CREATED        STATUS                    PORTS  NAMES
# ad2b786eeaba  hello-world  "/hello"  2 minutes ago  Exited (0) 1 minutes ago         kind_benz
```

Analyse : Le conteneur a été lancé, il a exécuté la commande "/hello", puis s'est arrêté. Son status est `Exited`.

Remarque : le Nom du conteneur a été choisi de façon aléatoire par le daemon Docker, ici "kind_benz".

### Supprimer le conteneur

Récupérer l'ID ou le nom du conteneur :

```bash
docker ps -a
```

Supprimer le conteneur 

```bash
docker rm <NOM du premier conteneur créé>
```

Vérifier le résultat

```bash
docker ps -a
```

---
## Astuces

### Astuce 1 : Gestion du nom des conteneur

Par défaut, Docker donne un nom aléatoire a chaque conteneur. Pour définir le nom d'un conteneur on utilise l'option `--name`

```bash
docker run --name  my-hello hello-world
docker ps -a
```

- le "NAME" du nouveau conteneur est "my-hello"
- si on relance la commande, on obtient une erreur `Conflict. The container name "/my-hello" is already in use by container ...` : on ne peut pas créer 2 conteneurs avec le même nom.
- intérêt : plus facile à manipuler

Supprimer le conteneur  

```bash
docker rm my-hello
```

### Astuce 2 : suppression automatique des conteneur après utilisation

L'option `--rm` permet de supprimer automatiquement le conteneur après utilisation.

```bash
docker run --rm --name my-hello-rm hello-world
docker ps -a
```

Il n'y a pas de conteneur "my-hello-rm" dans la liste, car il a été supprimé automatiquement.


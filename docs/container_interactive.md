# Travailler avec un conteneur en mode interactif

---
## Objectifs

- Comprendre le principe du mode interactif
- Lancer un conteneur en mode interactif
- Se connecter à un conteneur existant, en mode interactif

---
# Principe du mode interactif

Travailler en mode interactif revient dans la plupart des cas à ouvrir un shell sur un conteneur.

On utilise l'option `-it` pour pouvoir accéder à ce shell :

- `t` indique à Docker d'associer un pseudo-TTY au conteneur.
- `i` donne l'accès aux STDIN et STDOUT de ce terminal, afin de pouvoir saisir et afficher du contenu.

2 approches sont  possibles :

- Lancer directement un conteneur, en utilisant `run` 
- Se connecter à un conteneur  existant, en utilisant `exec`


Il faut bien sûr que le shell soit disponible dans le conteneur, ce qui n'est pas toujours le cas. 

On retrouve dans la plupart des cas `sh`, souvent `bash` (mais pas toujours), et parfois d'autres shell comme `ash`, ...

> Pour vérifier si un shell est disponible, lister le contenu du dossier `/bin`. 

Dans le cas ou aucun shell n'est disponible sur un conteneur, il est toujours possible d'utiliser la commande `attach`. Mais le comportement de cette commande dépend du fonctionnement du conteneur, et entraîne un risque d'arrêt du conteneur.  `attach` est donc à utiliser avec précautions. (*voir [docker attach](./container_attach.md)*). 

> Sauf cas très particulier, on n'utilise jamais la commande `ssh` pour se connecter à un conteneur. ssh est une commande client qui se connecte à un serveur. Un serveur est une application. Le principe de Docker est de ne lancer qu'une seule application par conteneur. Aucun serveur SSH n'est donc disponible dans un conteneur.

---
## Lancer un conteneur en mode interactif

Utilisation de la commande : `docker run -it <CONTAINER NAME or ID> /bin/sh`

Lancer un conteneur en mode interactif :
``` bash
docker run -it --name cont1  alpine /bin/bash
# docker: Error response from daemon: failed to create task for container: failed to create shim task: OCI runtime create failed: runc create failed: unable to start container process: exec: "/bin/bash": stat /bin/bash: no such file or directory: unknown.
```
Il y a un problème avec `/bin/bash` ...

Afficher les shells disponibles
```bash
docker run -t --rm alpine ls /bin
# ...
# ash
# ...
# sh
# ...
```

Le shell `bash` n'est effectivement pas disponible dans l'image alpine. Il faut donc utiliser soit `sh`, soit `ash`.

```bash
docker run -it --name cont1 alpine /bin/sh
# / # 
```
Cette fois ça fonctionne. Un terminal interractif a été associé au conteneur, et un shell à été lancé, d'où le changement du prompt `/ #`.

On peut lancer des commandes unix :

- Les commandes de base : `ls`, `df`, `top`, ...
- `hostname` pour afficher le nom du conteneur, qui correspond à son ID
- `hostname -i` pour afficher l'adresse IP du conteneur
- enfin, pour sortir du shell : `exit`

La session de  shell est liée à la vie du conteneur. Quand on sort du shell, avec la commande `exit`, le conteneur s'arrête et est détruit.

Avec la commande `docker ps -a`, on voit que le conteneur a été lancé, et resté actif tant qu'on était connecté, s'est arrêté quand la commande `exit` a été saisie ( durée entre `CREATED` et `Exited` )


Ménage :
``` bash
docker rm cont1
```
---
## Se connecter à un conteneur en cours d'execution

Utilisation de la commande : `docker exec -it <CONTAINER NAME or ID> /bin/sh`

Pour tester la connexion, lancer un conteneur en mode détaché :
```bash
docker run  --rm --name cont1 -d alpine sleep infinity
```

Si besoin, afficher les shells disponibles :
```bash
docker exec -t cont1 ls /bin
# ...
# ash
# ...
# sh
# ...
```

Puis se connecter au conteneur :
```bash
docker exec -it cont1 /bin/sh
# / # 
```

Il est alors possible de lancer les commandes disponibles, comme précédemment.


Par contre cette fois, la commande `exit` entraîne la déconnexion du terminal, mais le conteneur reste actif.

Avec la commande `docker ps`, on voit que le conteneur est toujours actif.

---
## Ménage

```bash
docker stop cont1
docker rm cont1
```
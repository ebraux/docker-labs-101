# Personnalisation d'une image avec COPY


Proposition de solution pour le lab [DockerFile COPY](./dockerfile_copy.md)

----
## Préparation

Créer un dossier "web_copy"
```bash
mkdir web_copy; cd web_copy
```

---
## Création du fichier html

Création du dossier pour le site
```bash
mkdir mysite;
```

Création du fichier html
```bash
tee  mysite/index.html <<EOF
<html>
  <body>
    <h1>My Site</h1>
  </body>
</html>
EOF
```

Et vérification
```bash
cat  mysite/index.html
```

---
## Création du Dockerfile

Le contenu web se trouve dans le dossier `/usr/share/nginx/html` dans l'image Nginx.

Créer un fichier Dockerfile contenant:

- Le nom de l'image source : mot clé `FROM`
- La copie du site vers le dossier `/usr/share/nginx/html` : mot clé `COPY`

> Informations sur les commandes qui peuvent être utilisées dans un Dockerfile : [https://docs.docker.com/engine/reference/builder/](https://docs.docker.com/engine/reference/builder/)

```
tee  Dockerfile <<EOF
FROM nginx:latest
COPY mysite /usr/share/nginx/html
EOF
```


Et vérification
```bash
cat Dockerfile
```

---
##  Builder l'image

Le build de l'image se fait en 3 étapes : 

- Étape 1 : récupération de l'image de base "nginx:latest"
- Étape 2 : copie du contenu vers le conteneur
- Étape 3: construction de la une nouvelle image, avec le nom "my_site:latest"

```bash
docker build -t  my_site .	   
# [+] Building 0.4s (7/7) FINISHED                                                                             
#  => [internal] load build definition from Dockerfile             0.0s
#  => => transferring dockerfile: 122B                             0.0s
#  => [internal] load .dockerignore                                0.0s
#  => => transferring context: 2B                                  0.0s
#  => [internal] load metadata for docker.io/library/nginx:latest  0.0s
#  => [internal] load build context                                0.1s
#  => => transferring context: 64B                                 0.0s
#  => [1/2] FROM docker.io/library/nginx:latest                    0.2s
#  => [2/2] COPY mysite /usr/share/nginx/html                      0.1s
#  => exporting to image                                           0.1s
#  => => exporting layers                                          0.1s
#  => => writing image sha256:7e928f0424ba ... b50fa4f5a064f05f5b  0.0s
#  => => naming to docker.io/library/my_site                       0.0s
```

- L'option `-t` sert à nommer l'image
- Le point à la fin de la ligne indique qu'on travaille dans le dossier courant



Affichage des images
```bash
docker image ls
# REPOSITORY    TAG       IMAGE ID       CREATED          SIZE
# my_site       latest    7e928f0424ba   2 minutes ago   142MB
# nginx         latest    448a08f1d2f9   2 weeks ago     142MB
```

On retrouve l'image "my_site"

---
## Lancement d'un conteneur pour test

Lancement d'un conteneur avec l'image "my_site" personnalisée :
```language
docker run -d --rm --name c-my-site -p 8081:80 my_site
```
Et accès au site via un navigateur : [http://localhost:8081](http://localhost:8081)

Ou avec curl
``` bash
curl http://localhost:8081
```

Une page contenant le message "My Site" doit s'afficher.

---
## Ménage


Suppression du dossier :
```bash
cd ..
rm -rf web_copy
```
Suppression du conteneur:
```bash
docker stop c-my-site
```

Suppression de l'image : 
```bash
docker rmi my_site
```


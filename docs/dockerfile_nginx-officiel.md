# Étude de l'image "Nginx"

Image dans le Docker Hub :

- [https://hub.docker.com/_/nginx](https://hub.docker.com/_/nginx)

Dépôt Git associé : 

- [https://github.com/nginxinc/docker-nginx/](https://github.com/nginxinc/docker-nginx/)
- [https://github.com/nginxinc/docker-nginx/tree/master/stable/debian](https://github.com/nginxinc/docker-nginx/tree/master/stable/debian)


Dockerfile :

- Un seul `RUN`
- Utilisation de   `\` pour les retour à la ligne et `&&` pour passer à la commande suivante
- Purge des listes de "package" et du cache
- Redirection de logs vers les sorties standards
- Utilisation de `ENTRYPOINT` pour l'initialisation
- Utilisation de `CMD` pour lancer nginx
- `Nginx` lancé en "mode processus standard", et pas en mode "daemon"

docker-entrypoint.sh:

- Utilisation d'un dossier `entrypoint.d`
- Se termine par `exec "$@"`
- Utilisation de la commande `envsubst` 
- Optimisation de la configuration de Nginx
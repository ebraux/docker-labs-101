

docker inspect c-net-test
docker network inspect -f '{{json .Containers}}' net-test | jq '.[] | .Name + ":" + .IPv4Address'
docker inspect -f '{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}'  c-net-test
docker inspect -f '{{json .NetworkSettings.Networks}}'  c-net-test | jq

 get the number of restarts for container “my-container”;
docker inspect -f "{{ .RestartCount }}" my-container


 get the last time the container was (re)started;
docker inspect -f "{{ .State.StartedAt }}" my-container


 docker ps --format '{{.Command}} {{.Ports}}'
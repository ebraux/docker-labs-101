# Execution d'une commande avec un conteneur Docker : syntaxe avancée

---
## Objectifs

- Utiliser un conteneur Docker pour exécuter une commande, contenant un pipe, une redirection, ou une variable

Pour plus d'informations sur le lancement d'une commande avec Docker, voir *[Execution d'une commande avec un conteneur Docker](./container_command.md)*

---
## Principe

Pour lancer une commande utilisant une redirection, ou un pipe, il faut encapsuler la commande dans un appel de shell, avec `sh -c " COMMANDE "`

---
## Utilisation avec une redirection

- Lancer un conteneur :
``` bash
docker run -td --name cont1 alpine
```
- Lancer une commande dans le conteneur, pour créer un fichier, en utilisant une redirection
``` bash
docker exec cont1  echo 'toto' >  /tmp/toto.txt
```
- Afficher le fichier
``` bash
docker exec cont1 cat  /tmp/toto.txt
# cat: can't open '/tmp/toto.txt': No such file or directory
```

La commande `echo 'toto'` a bien été exécutée sur le conteneur, mais la redirection s'est appliquée sur la sortie standard de la commande Docker. Le fichier a donc été créé en local:
``` bash
cat  /tmp/toto.txt
# toto
```

Ménage :
``` bash
rm  /tmp/toto.txt
```

Si on encapsule la commande dans un appel de shell, l'intégralité de la commande est exécutée dans le conteneur, et donc le fichier `/tmp/toto.txt` est bien crée dans le conteneur :
- Création du fichier
``` bash
docker exec cont1  sh -c "echo 'toto' >  /tmp/toto.txt"
```
- Affichage du fichier dans le conteneur
``` bash
docker exec cont1 cat  /tmp/toto.txt
# toto
```
- Vérification qu'il n'a pas été créé en local cette fois
``` bash
cat  /tmp/toto.txt
# cat: /tmp/toto.txt: No such file or directory
```

Ménage
```bash
docker rm -f  cont1 
```

---
##  Utilisation avec un pipe 

Par exemple, pour afficher la ligne du fichier "/etc/password", correspondant à l'utilisateur en cours, on peut utiliser la commande :
``` bash
cat  /etc/passwd | grep $(id -un)
```

- La commande `cat  /etc/passwd` va permettre d'afficher le contenu du fichier "/etc/passwd"
- La commande `id -un` va permettre d'afficher l'utilisateur en cours
- Pour utiliser la sortie standard d'une commande comme une variable, on place cette commande entre parenthèses, précédée du signe "$" : ` $(id -un)`
- La commande `grep` permet, entre autre, de filtrer le contenu de la sortie standard pour n'afficher que les lignes contenant une chaîne de caractères
- Le "pipe", permet d'envoyer la sortie d'une commande, vers l'entrée d'une autre commande

Exemple d'exécution de la commande sur la machine locale :
``` bash
cat  /etc/passwd | grep $(id -un)
# ubuntu:x:1000:1000:ubuntu,,,:/home/ubuntu:/bin/bash
```

Cette commande lancée sur un conteneur ne renvoie pas de résultat.
``` bash
docker run --rm --name cont1 alpine  cat  /etc/passwd | grep $(id -un)
```

- La commande `cat  /etc/passwd` est exécutée dans le conteneur "cont1"
- La commande `grep $(id -un)` est exécutée sur la machine locale
    - Le grep va bien traiter le contenu du fichier issu du conteneur
    - Mais la commande `id -un` renvoie le nom de l'utilisateur courant sur la machine locale, qui n'est pas dans ce fichier, car il n'existe pas dans le conteneur.

Pour que l'intégralité de la commande soit exécutée sur le conteneur, il faut encapsuler la commande dans un appel de shell. Il faut également dans la cas présent neutraliser le caractère ` $`, sinon ` $(id -un)` sera interprété au lancement de la commande sur la machine locale, et non sur dans le conteneur, ce qui provoquera une erreur :
``` bash 
docker run --rm --name cont1 alpine sh -c "cat  /etc/passwd | grep \$(id -un)"
# root:x:0:0:root:/root:/bin/sh
```



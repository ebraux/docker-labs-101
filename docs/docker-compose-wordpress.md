# Exemple de déploiement de Wordpress


---
## Description

WordPress est un outil Opensource de gestion de blog et gestion de contenu (CMS). Il est basé sur PHP et MySQL. [https://wordpress.org/](https://wordpress.org/)

Objectif : déployer l'application "Wordpress", composée d'un conteneur "Wordpress", et d'un conteneur "Mysql", en utilisant docker-compose.


En s'appuyant sur l'image docker officielle Wordpress [https://hub.docker.com/_/wordpress](https://hub.docker.com/_/wordpress), et l'exemple de fichier docker-compose proposé dans la description de cette image.


``` yaml
version: '3.1'

services:

  wordpress:
    image: wordpress
    restart: always
    ports:
      - 8080:80
    environment:
      WORDPRESS_DB_HOST: db
      WORDPRESS_DB_USER: exampleuser
      WORDPRESS_DB_PASSWORD: examplepass
      WORDPRESS_DB_NAME: exampledb
    volumes:
      - wordpress:/var/www/html

  db:
    image: mysql:5.7
    restart: always
    environment:
      MYSQL_DATABASE: exampledb
      MYSQL_USER: exampleuser
      MYSQL_PASSWORD: examplepass
      MYSQL_RANDOM_ROOT_PASSWORD: 'yes'
    volumes:
      - db:/var/lib/mysql

volumes:
  wordpress:
  db:
```

**Cette stack :**

- Déploie 2 services : "wordpress" et "db"
- Utilise 2 volumes pour la pesristance des données : "wordpress" et "db"


**Le service "db" :**

- Se base sur l'image standard "mysql", en version "5.7"
- Ne publie pas de port. Ce n'est pas nécessaire, car :
    - Les utilisateurs n'ont pas besoin d'accéder directement à la base de données
    - Les échanges entre le service "Wordpress", et le service "db" se font sur le réseau interne de la stack
- Transmet au conteneur les variables d'environnement pour :
    - La configuration de mysql
    - La configuration d'une base de données et d'un utilisateur associé
    - Voir la documentation de l'image pour plus de détails sur ces variables [https://hub.docker.com/_/mysql](https://hub.docker.com/_/mysql)
- Utilise le volume "db" pour stocker de façon persistante la base de données (dossier "/var/lib/mysql" dans le conteneur)
    - le volume est initialisé par le contenu du dossier "/var/lib/mysql" du conteneur lors du premier lancement de la stack.

**Le service "Wordpress" :**

- Se base sur l'image standard "wordpress", en version "latest"
- Publie le port 8080 sur la machine hôte, redirigé vers le port 80 du conteneur
- Transmet au conteneur les variable d'environnement pour la configuration de l'accès à la base de données
    - Noter l'utilisation de "db" en tant que `WORDPRESS_DB_HOST`, qui fait référence au nom du service "db"
    - Voir la documentation de l'image pour plus de détails sur ces variables [https://hub.docker.com/_/wordpress](https://hub.docker.com/_/wordpress)
- Utilise le volume "wordpress" pour stocker de façon persistante le contenu du site (dossier "/var/www/html" dans le conteneur)
    - le volume est initialisé par le contenu du dossier "/var/www/html" du conteneur lors du premier lancement de la stack.


---
## Déploiement

Lancer la stack
``` bash
docker compose up
# [+] Running 5/5
#  ✔ Network wordpress_default        Created        0.1s 
#  ✔ Volume "wordpress_wordpress"     Created        0.0s 
#  ✔ Volume "wordpress_db"            Created        0.0s 
#  ✔ Container wordpress-db-1         Created        0.1s 
#  ✔ Container wordpress-wordpress-1  Created        0.1s
# ...
```

- Observer les logs de démarrage.
  - Du service Wordpress `wordpress-wordpress-1 ...`
  - Du service Mysql `wordpress-db-1 ...`
- Se connecter à wordpress [http://localhost:8080](http://localhost:8080)

Arrêter la stack avec ++Ctrl+C+++
``` bash
Gracefully stopping... (press Ctrl+C again to force)
# Aborting on container exit...
# [+] Running 2/2
#  ✔ Container wordpress-wordpress-1  stopped    1.3s 
#  ✔ Container wordpress-db-1         Stopped    1.8s
#  canceled
```

Afficher les volume Docker
``` bash
docker volume ls
# DRIVER    VOLUME NAME
# local     wordpress_db
# local     wordpress_wordpress
```

Lancer la stack en arrière plan
``` bash
docker compose up -d
# [+] Running 2/2
#  ✔ Container wordpress-db-1         Started        0.4s 
#  ✔ Container wordpress-wordpress-1  Started           
```

Afficher les informations sur la stack lancée :
``` bash
docker compose ls
# NAME                STATUS              CONFIG FILES
# wordpress           running(2)          docker-compose.yml
```

Afficher les informations sur les services lancés :
``` bash
docker compose ps --services
# db
# wordpress
```

Afficher les informations sur les conteneurs lancés :
``` bash
docker compose ps
# NAME                   IMAGE      COMMAND                 SERVICE    CREATED        STATUS        PORTS
# wordpress-db-1         mysql:5.7  "docker-entrypoint.s…"  db         5 minutes ago  Up 5 minutes  3306/tcp, 33060/tcp
# wordpress-wordpress-1  wordpress  "docker-entrypoint.s…"  wordpress  5 minutes ago  Up 5 minutes  80/tcp
```

Afficher les logs
``` bash
docker compose logs  
docker compose logs -f
```

Arrêter la stack
``` bash
docker compose down
# [+] Running 3/3
#  ✔ Container wordpress-db-1         Removed         2.1s 
#  ✔ Container wordpress-wordpress-1  Removed         1.3s 
#  ✔ Network wordpress_default        Removed         0.4s
```


---
## Ménage

Arrêter la stack, et demander la suppression des volumes
``` bash
docker compose down -v
# [+] Running 2/0
#  ✔ Volume wordpress_wordpress  Removed         0.0s 
#  ✔ Volume wordpress_db         Removed         0.1s
```
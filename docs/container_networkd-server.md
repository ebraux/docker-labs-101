
# Lancement d'un conteneur en mode détaché accessible via le réseau

---
## Principe

Utilisation par exemple pour lancer un service de type serveur WEB. Utilisation d'une image de base "Nginx".

Le serveur Nginx écoute sur le port "80" dans le conteneur. Mais il n'est pas accessible depuis la machine hôte.

Il est nécessaire d'indiquer à Docker de rendre le port du conteneur accessible depuis la machine hôte.

Docker va mettre en place une redirection de port, entre un port de la machine hôte (8081 par exemple), vers le port 80 du conteneur Nginx.

C'est ce que fait l'option `-p`.

`-p 8081:80` va indiquer à Docker d'écouter sur le port 8081, et de rediriger le flux vers le port 80 du conteneur. Format : `Host port:container port`.

---
## Lancement d'un conteneur de type srveur WEB : Nginx

Lancement d'un conteneur, à partir d'une image Nginx : 
``` bash
docker run --rm --name cont1 -d nginx
docker ps 
```

La commande `docker ps` affiche des informations sur le conteneur, notamment que le conteneur propose un service qui écoute sur le port 80 : `PORTS : 80/tcp`

Test: accéder au serveur Nginx avec curl, ou un navigateur, en utilisant l'url L'accès [http://localhost:80](http://localhost:80).
``` bash
curl http://localhost:80
```
La connexion échoue.

La même commande lancée dans le conteneur affiche du contenu en HTML :
``` bash
docker exec cont1 curl http://localhost:80
```

Le serveur Nginx est donc accessible depuis le conteneur, mais pas depuis la machine hôte. 

Pour pouvoir accéder à ce service, il faut "exposer" le port 80 du conteneur sur un port au niveau de l'hôte.

Ménage : 
``` bash
docker stop cont1
```

> Le conteneur est supprimé automatiquement, car l'option `--rm` a été utilisée.

---
## Lancement du conteneur, avec exposition du port

On reprend le même exemple, en utilisant l'option `-p`, qui indique à Docker de publier le service au niveau de la machine hôte :
``` bash
docker run --rm --name cont1 -d -p 8081:80 nginx
docker ps
```
Cette fois les informations de connexion réseau indiquent que les requêtes vers le port 8081 de l'hôte seront redirigées vers le port 80 du conteneur :`PORTS: 0.0.0.0:8081->80/tcp`

Le test d'accès au serveur Nginx avec curl ou un navigateur, en utilisant le port 8081 doit fonctionner : [http://localhost:8081](http://localhost:8081)

``` bash
curl http://localhost:8081
```

---
## Ménage

``` bash
docker stop cont1
docker rmi nginx
```
# Exemple de scaling de services


---
## Scaling

Reprendre la stack wordpress déployée dans le lab [Exemple de déploiement de Wordpress](docker-compose-wordpress.md)


Lancer la stack en arrière plan
``` bash
docker compose up -d
```

Afficher les informations sur les services lancés :
``` bash
docker compose ps --services
# db
# wordpress
```

Afficher les informations sur les conteneurs lancés :
``` bash
docker compose ps
# NAME                   IMAGE      COMMAND                 SERVICE    CREATED        STATUS        PORTS
# wordpress-db-1         mysql:5.7  "docker-entrypoint.s…"  db         5 minutes ago  Up 5 minutes  3306/tcp, 33060/tcp
# wordpress-wordpress-1  wordpress  "docker-entrypoint.s…"  wordpress  5 minutes ago  Up 5 minutes  80/tcp
```


Scaler le service wordpress
``` bash
docker-compose up -d --scale wordpress=2
```

Le scaling échoue
``` bash
wordpress_db_1 is up-to-date

WARNING: The "wordpress" service specifies a port on the host.
  If multiple containers for this service are created on a single host, the port will clash.
Creating wordpress_wordpress_2 ... 
Creating wordpress_wordpress_2 ... error

ERROR: for wordpress_wordpress_2  Cannot start service wordpress: 
  driver failed programming external connectivity on endpoint wordpress_wordpress_2 
  Bind for 0.0.0.0:8080 failed: port is already allocated
...
```

Le service `wordpress` publie le port 8080. Plusieurs services ne peuvent pas utiliser le même port :

- Désactiver la publication du port et relancer le scaling
- Utiliser un nouveau service en frontend : nginx par exemple.

# Personnalisation d'une image : Serveur Nginx

---
## Objectifs

Créer une image personnalisée :

- A partir d'une image "debian:bullseye-slim"
- Créer une image permettant de lancer le serveur "Nginx"

---
## Principe 

- Écrire un fichier de personnalisation : **Dockerfile**
- Installer les paquets Nginx
- Modifier la commande par défaut, pour lancer Nginx en mode `daemon off`
- Construire une nouvelle image.


Les commandes nécessaires pour installer les paquets Nginx sont :
``` bash
# Mise à jour de la liste des paquets
apt-get update
# Installation
apt-get install -y nginx
# Ménage
apt-get remove --purge --auto-remove -y 
rm -rf /var/lib/apt/lists/* 
```

La commande pour lancer Nginx est : `/usr/sbin/nginx -g 'daemon off;'`

> Le serveur Nginx dans une distribution Debian publie le contenu du dossier `/var/www/nginx`.

---
## Solution

- [Proposition de solution](./dockerfile_nginx_solution.md)


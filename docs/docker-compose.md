# Utilisation de Docker compose


## Lancement d'un conteneur simple

### Lancer le conteneur manuellement

Un conteneur qui attend 10 secondes, et s'arrête ;
```bash
docker run --rm --name wait10 alpine sleep 10
```


### L'équivalent avec Docker-compose.

Éléments pour docker-compose :

- Nom du service : wait10
- Nom de l'image : alpine
- Commande à execution : `sleep 10`

Créer un fichier docker-compose.yml permettant de lancer ce service :
``` bash
mkdir compose_wait; cd compose_wait

tee docker-compose.yml <<EOF
version: "3.7"
services:
  wait10:
    image: alpine
    command: "sleep 10"
EOF

```

Lancer le conteneur (la stack)
``` bash
docker compose up 
# [+] Running 2/2
#  ✔ Network compose_wait_default     Created                                                     0.1s 
#  ✔ Container compose_wait-wait10-1  Created                                                     0.1s 
# Attaching to compose_wait-wait10-1
```

Et au bout de 10 secondes
``` bash
# compose_wait-wait10-1 exited with code 0
```

Cette commande :

- Recherche le fichier docker-compose.yml dans le dossier courant
- Crée un réseau "compose_wait_default" spécifique pour la stack 
- Lance les conteneurs définis en tant que service, ici "compose_wait_wait10_1" pour le service "wait10"
- Attend 10 secondes et s'arrête


Ménage
``` bash
docker-compose down
# Removing compose_wait-wait10-1 ... done
# Removing network compose_wait_default
```

Cette commande :

- Supprime le servie wait10, et donc ici le conteneur "compose_wait_wait10_1"
- Supprime le réseau compose_wait_default

---
## Lancement d'un conteneur Nginx (serveur WEB)

### Lancer le conteneur manuellement


Commande Docker permettant de lancer un conteneur Nginx, en tant que daemon (option `-d`), et en publiant le port 8081 (option `-p 8081:80`) :

```bash
docker run \
  -d \
  --rm \
  --name web \
  -p 8081:80 \
  nginx
```

Vérification
```bash
docker ps
CONTAINER ID   IMAGE     COMMAND                  CREATED          STATUS          PORTS                                   NAMES
66fd9d207d1a   nginx     "/docker-entrypoint.…"   12 seconds ago   Up 10 seconds   0.0.0.0:8081->80/tcp, :::8081->80/tcp   web
```

Test de connexion :
```bash
curl http://localhost:8081
# <!DOCTYPE html>
# <html>
# <head>
# <title>Welcome to nginx!</title>
# <style>
#  ...
#  <h1>Welcome to nginx!</h1>
# <p>If you see this page, the nginx web server is successfully installed and
# working. Further configuration is required.</p>
# ...
# </html>
```

Arrêt du conteneur
```bash
docker stop web
```

### L'équivalent avec Docker-compose.

Créer un fichier docker-compose.yml permettant de lancer Nginx

``` bash
mkdir compose_nginx; cd compose_nginx

tee docker-compose.yml <<EOF
version: "3.7"
services:
  web:
    image: nginx
    restart: always
    ports:
      - 8081:80
EOF
```

Lancer Nginx
``` bash
docker compose up 
# [+] Running 2/2
#  ✔ Network compose_nginx_default  Created                                                       0.1s 
#  ✔ Container compose_nginx-web-1  Created                                                       0.1s 
# Attaching to compose_nginx-web-1

```

Cette commande :

- Recherche le fichier docker-compose.yml dans le dossier courant
- Créer un réseau "compose_nginx_default" spécifique pour le stack 
- Lance les conteneurs définis en tant que service, ici "compose_nginx_web_1" pour le service "web"
- Affiche les messages d'initialisation sur la sortie standard

Si le lancement s'est bien déroulé, les logs du conteneur "nginx" doivent s'afficher :
```bash
# ...
# compose_nginx-web-1  | /docker-entrypoint.sh: Configuration complete; ready for start up
# ...
# compose_nginx-web-1  | 2023/05/17 07:37:12 [notice] 1#1: start worker processes
# ...
```

> Rem : le format du conteneur lancé pour un service est "Nom_du_dossier_courant"_"Nom_du_service"_"indice" (plusieurs instances du même conteneur peuvent être lancées pour un service)

Arrêter le conteneur avec ++Ctrl+C++
``` bash
# ^CGracefully stopping... (press Ctrl+C again to force)
# Aborting on container exit...
# [+] Running 1/1
#  ✔ Container compose_nginx-web-1  Stopped                                                                                                                                                                 0.4s 
# canceled
```

Relancer la stack Nginx en mode détaché
``` bash
docker compose up -d
# Recreating compose_nginx-web-1 ... done
```

Et effectuer le test de connexion. Le contenu de la page d'accueil de Nginx doit s'afficher, comme précédemment :
```bash
curl http://localhost:8081
```

Supprimer la stack :
``` bash
docker compose down
# [+] Running 2/2
#  ✔ Container compose_nginx_web_1  Removed                                                                                                                                                                 0.5s 
#  ✔ Network compose_nginx_default  Removed                              
```

Cette commande :

- Supprime le conteneur "compose_nginx-web-1"
- Supprime le réseau spécifique "compose_nginx_default"



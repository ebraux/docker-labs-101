# Execution d'une commande avec un conteneur Docker

---
## Objectifs

- Utiliser un conteneur Docker pour exécuter une commande.

---
## Lancement d'une commande via un conteneur

Le conteneur utilise comme image de base une image `alpine` : système Linux minimaliste, très utilisé comme base pour les conteneurs.

Cette image permet de lancer des commande Linux. La commande `echo "hello world"`, va être utilisée, elle permet d'afficher le message  "Hello World".


```bash
docker run --rm --name cont1 alpine echo 'hello world'
```

- Le message "hello word" s'affiche sur la console. c'est le résultat de la commande `echo hello world`
- Avec `docker ps -a`, on voit :
    - Que le conteneur a exécuté la commande  "echo 'hello world'" au lieu de "/bin/sh"
    - Qu'il s'est lancé et s'est arrêté.




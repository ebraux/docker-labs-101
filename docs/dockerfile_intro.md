# Personnalisation d'une image

---
## Objectifs

Créer une image personnalisée :

- A partir d'une image "alpine"
- Qui affiche le message "Hello World"

---
## Principe 

Pour personnaliser une image :

- Écrire un fichier de personnalisation : **Dockerfile**
- Construire une nouvelle image.

---
## Création du Dockerfile

Créer un dossier "my_hello" et y créer un fichier Dockerfile contenant:

- Le nom de l'image source : mot clé `FROM`
- La configuration de la commande à exécuter : mot clé `CMD`

> Informations sur les commandes qui peuvent être utilisées dans un Dockerfile : [https://docs.docker.com/engine/reference/builder/](https://docs.docker.com/engine/reference/builder/)

```bash
mkdir my_hello; cd my_hello

tee -a Dockerfile <<EOF
FROM alpine:latest
CMD ["/bin/echo", "Hello World"]
EOF

```

---
##  Builder l'image

Le build de l'image se fait en 3 étapes : 

- Étape 1 : récupération de l'image de base "alpine:latest"
- Étape 2 : configuration de la commande par défaut du conteneur,
- Étape 3: construction de la nouvelle image, avec le nom "my_hello:latest"

```bash
docker build -t  my_hello .	   
# Sending build context to Docker daemon  2.048kB
# Step 1/2 : FROM alpine:latest
#  ---> 14119a10abf4
# Step 2/2 : CMD ["/bin/echo", "Hello World"]
#  ---> Running in ca7e02055b5c
# Removing intermediate container ca7e02055b5c
#  ---> 30c51bd2b430
# Successfully built 30c51bd2b430
# Successfully tagged my_hello:latest
```

- L'option `-t` sert à nommer l'image
- Le point à la fin de la ligne indique qu'on travaille dans le dossier courant



Affichage des images
```bash
docker image ls
# REPOSITORY    TAG       IMAGE ID       CREATED          SIZE
# my_hello      latest    30c51bd2b430   47 seconds ago   5.61MB
```

On retrouve l'image "my_hello".

---
## Lancement d'un conteneur pour test

Lancement d'un conteneur avec l'image "my_hello" personnalisée :
``` bash
docker run --name test-my-hello  my_hello
```

Afficher les information sur le container :
``` bash
docker ps -a
# CONTAINER ID   IMAGE      COMMAND                  CREATED          STATUS                      PORTS     NAMES
# 99e2c1fdbf78   my_hello   "/bin/echo 'Hello Wo…"   39 seconds ago   Exited (0) 38 seconds ago             test-my-hello
```
On retrouve bien la commande lancée : `"/bin/echo 'Hello Wo…"`

---
## Ménage


Suppression du dossier :
```bash
cd ..
rm -rf my_hello
```

Suppression du conteneur :
```bash
docker rm test-my-hello
```

Suppression de l'image : 
```bash
docker rmi my_hello
```


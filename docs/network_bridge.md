# Utilisation des réseaux Bridge

## Objectifs

- Comprendre la configuration réseau par défaut
- Créer un réseaux, y attacher des conteneur, et observer le lien entre les objets Docker

---
## Créer un conteneur, et observer la configuration réseau par défaut

### Création d'un conteneur

Créer un conteneur :
``` bash
docker run -t -d --rm --name c-net-test  alpine
```

Observer la connexion réseau
``` bash
docker inspect c-net-test
```

Et particulièrement :

- Son adresse IP
``` bash
docker inspect -f '{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}'  c-net-test
# 172.17.0.2
```
- La configuration réseau : 
``` bash
docker inspect -f '{{json .NetworkSettings.Networks}}'  c-net-test | jq
```
``` json
{
  "bridge": {
    "IPAMConfig": null,
    "Links": null,
    "Aliases": null,
    "NetworkID": "24414094a47e35cf7a721b836aa6fdb3ac515f3305ab49783891418fca9b3aca",
    "EndpointID": "3a75a84d95068e7c18362a7ee4e92d5c76d0690fde2134748124e367d1d0eeba",
    "Gateway": "172.17.0.1",
    "IPAddress": "172.17.0.2",
    "IPPrefixLen": 16,
    "IPv6Gateway": "",
    "GlobalIPv6Address": "",
    "GlobalIPv6PrefixLen": 0,
    "MacAddress": "02:42:ac:11:00:02",
    "DriverOpts": null
  }
}
```

**Le conteneur est connecté au réseau "bridge", et son adresse IP est "172.17.0.2".**


### Observer le réseau "bridge"

Afficher les informations sur le réseau "bridge"
``` bash
docker inspect bridge
```

Et plus précisément :

- la configuration du réseau
``` bash
docker inspect -f '{{json .IPAM.Config}}'  bridge| jq
```
``` json
[
  {
    "Subnet": "172.17.0.0/16",
    "Gateway": "172.17.0.1"
  }
]
```
- Les conteneurs connectés à ce réseau
``` bash
docker inspect -f '{{json .Containers}}'  bridge| jq
```
``` json
{
  "bb77ac00f737ff536fceb7064e7d92fc15bca78db4e5440a73ccec1530a16a2a": {
    "Name": "c-net-test",
    "EndpointID": "3a75a84d95068e7c18362a7ee4e92d5c76d0690fde2134748124e367d1d0eeba",
    "MacAddress": "02:42:ac:11:00:02",
    "IPv4Address": "172.17.0.2/16",
    "IPv6Address": ""
  }
}
```

**Le réseau "bridge" utilise le CIDR 172.17.0.0/16. Un conteneur y est connecté. L'adresse 172.17.0.2/16 lui a été attribuée.**

---
## Création d'un réseau privé


Afficher les réseaux:

``` bash
docker network ls
# NETWORK ID     NAME                   DRIVER    SCOPE
# 24414094a47e   bridge                 bridge    local
# b1864827172a   host                   host      local
# 0c5fa0b4108b   none                   null      local
```


Créer un réseau :
``` bash
docker network create net-test
```

``` bash
docker network ls
# NETWORK ID     NAME                   DRIVER    SCOPE
# 24414094a47e   bridge                 bridge    local
# b1864827172a   host                   host      local
# 0c5fa0b4108b   none                   null      local
# faadf9b0f2f6   net-test               bridge    local
```


Afficher les informations du réseau : 
``` bash
docker inspect --format='{{.Driver}}' net-test
```

Ou uniquement les parties spécifiques : 

- Type de driver
``` bash
docker inspect --format='{{.Driver}}' net-test
# bridge
```
- Configuration IP
``` bash
docker inspect --format='{{json .IPAM.Config}}' net-test | jq
# [
#  {
#    "Subnet": "172.25.0.0/16",
#    "Gateway": "172.25.0.1"
#  }
# ]
```

---
## Observer le lien avec les conteneurs

Lancer 2 conteneurs attachés à ce réseau
``` bash
docker run -d --rm --name c-net-test1 --network net-test alpine sleep infinity
docker run -d --rm --name c-net-test2 --network net-test alpine sleep infinity
```

Et afficher leurs adresses IP :
``` bash
docker inspect -f '{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}'  c-net-test1
# 172.25.0.2

docker inspect -f '{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}'  c-net-test2
# 172.25.0.3

```

Afficher les informations sur les conteneurs connectés au réseau :
``` bash
docker network inspect -f '{{json .Containers}}' net-test | jq 
# {
#   "629e16276e741d235da6ce82bf9d90d49a855d4e22793ccdde0c53356741c947": {
#      "Name": "c-net-test1",
#      "EndpointID": "c45c3000f00d170955718aa84049a590ee8da81d8cd3c8b5ed78f49e0002848a",
#      "MacAddress": "02:42:ac:19:00:02",
#      "IPv4Address": "172.25.0.2/16",
#      "IPv6Address": ""
#   },
#   "77199ea44d31b41b2c4f2f10fdc40e8db4211283af1d50e00d10d4025ac67101": {
#      "Name": "c-net-test2",
#      "EndpointID": "52666b69c2b1821abcff655a9bd8f0fa462844aaa05b5a4856bf8bfab426133c",
#      "MacAddress": "02:42:ac:19:00:03",
#      "IPv4Address": "172.25.0.3/16",
#      "IPv6Address": ""
#   }
# }
```

Pour n'afficher que la liste des adresses IP des conteneurs attachés au réseau :
``` bash
docker network inspect -f '{{json .Containers}}' net-test | jq '.[] | .Name + ":" + .IPv4Address'
# "c-net-test1:172.25.0.2/16"
# "c-net-test2:172.25.0.3/16"
```

## Ménage

``` bash
docker stop c-net-test1 c-net-test2
docker network rm net-test
``` 
